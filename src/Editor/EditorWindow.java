package Editor;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.*;

import NeuralNet.NeuralNet;

public class EditorWindow extends JFrame implements ActionListener
{
    private RenderingCanvas canvas;
    private Button button, button2, button3;
    private NeuralNet network;
    
    final JFrame frame = new JFrame("Centered");
    String fileToLoad;
    boolean loaded = false;

    public EditorWindow(String title, int width, int height)
    {

        setTitle(title);
        setSize(width, height);
        setLocation(500, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        canvas = new RenderingCanvas(600, 600);
        button = new Button("Compute");
        button.addActionListener(new ActionListener() {
        	@Override
            public void actionPerformed(ActionEvent e)
            {
        		if (!loaded) {
        		ArrayList<String> percents = new ArrayList<String>(10) ;
        		int temp[] = new int[10];
        		int total = 0;
        		Screen screen = canvas.getScreen();

                List<Double> results = network.predict(screen.getNormalizedPixels());
                for(int i=0; i < 10; i++) {
                	temp[i] = (int) (results.get(i)*100000);
                	total += temp[i];
                }
                System.out.println(total);
                for(int i=0; i < 10; i++) {
                	percents.add(((double)((int)(((double)temp[i] / total)*10000))/100) + "%");              	
                }
                System.out.println(percents);

                return;
            
        		}
        		System.out.println("No weights loaded");
        	}
        });
        button2 = new Button("Erase");
        button2.addActionListener(new ActionListener() {
        	@Override
            public void actionPerformed(ActionEvent e)
            {
                for(int i = 0; i < 28; i++) {
                	for(int j = 0; j < 28; j++) {
                	
                	canvas.screen.pixels[i][j] = new ColorVector(0,0,0);
                	}}
                canvas.updateImage();
            }
        });
        button3 = new Button("Load File");
        button3.addActionListener(this);

        Thread t = new Thread(canvas);
        t.start();
        add(canvas, BorderLayout.CENTER);
        add(button, BorderLayout.EAST);
        add(button2,BorderLayout.SOUTH);
        add(button3, BorderLayout.NORTH);
        
       

        setVisible(true);

    }





    public void setup(NeuralNet net)
    {
        network = net;
    }
    
    private static String createFileChooser(final JFrame frame) {
		 
    	String filename = File.separator+"tmp";
        JFileChooser fileChooser = new JFileChooser(new File(filename));
        FileFilter filter = new FileNameExtensionFilter("Neural Network Weights", ".nnw");
        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.setFileFilter(filter);
 
        // pop up an "Open File" file chooser dialog
        fileChooser.showOpenDialog(frame);
 
       
        if (fileChooser.getSelectedFile() == null) {
            System.out.println("Null File");
            return null;
        }

        System.out.println("File to open: " + fileChooser.getSelectedFile() + " Path: " + fileChooser.getSelectedFile().getAbsolutePath());
    	return fileChooser.getSelectedFile().getAbsolutePath();
 
    }

    
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
        
    	fileToLoad = (createFileChooser(frame));
    	
    	if(fileToLoad == null) return;
    	loaded = true;
    	

    }
}