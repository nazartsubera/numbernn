package Editor;

import javax.swing.*;
import javax.swing.event.MouseInputListener;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;

public class RenderingCanvas extends JPanel implements Runnable, MouseListener, MouseMotionListener
{
    public Screen screen;
    private BufferedImage image;
    private int width, height;
    private int mouseX, mouseY;
    private boolean mouseDown;
    private boolean hovered;



    private ImageObserver observer = new ImageObserver()
    {
        @Override
        public boolean imageUpdate(Image img, int infoflags, int x, int y, int width, int height)
        {
            return false;
        }
    };

    public RenderingCanvas(int width, int height)
    {
        this.width = width;
        this.height = height;

        image = new BufferedImage(28, 28, BufferedImage.TYPE_INT_RGB);

        addMouseMotionListener(this);
        addMouseListener(this);

        screen = new Screen(28, 28);

        updateImage();

    }

    @Override
    public void run()
    {
        while (true)
        {

            repaint();
        }
    }



    @Override
    public void paint(Graphics g)
    {

        Graphics2D g2D = (Graphics2D) g;


        g2D.drawImage(image, 0, 0, width, height, observer);
        if (mouseDown && hovered)
        {
            int x = Math.round(mouseX * 28/600);
            int y = Math.round(mouseY * 28/600);

            screen.pixels[x][y] = new ColorVector(255, 255, 255);
        }

    }

    public void updateImage()
    {

        Graphics2D graphics2D = ((Graphics2D)image.getGraphics());
        for (int i = 0; i < 28; i++)
        {
            for (int j = 0; j < 28; j++)
            {
                graphics2D.setColor(new Color(screen.pixels[i][j].r,screen.pixels[i][j].g ,screen.pixels[i][j].b));
                graphics2D.fillRect(i, j, 1, 1);
            }
        }
    }

    public Screen getScreen() {
        return screen;
    }

    public BufferedImage getImage() {
        return image;
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        mouseX = e.getX();
        mouseY = e.getY();


        if (mouseDown)
            updateImage();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        mouseX = e.getX();
        mouseY = e.getY();

        if (mouseDown)
            updateImage();
    }

    @Override
    public void mouseClicked(MouseEvent e)
    {
    }

    @Override
    public void mousePressed(MouseEvent e)
    {
        if (e.getButton() == 1)
        {
            mouseDown = true;
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (e.getButton() == 1)
        {
            mouseDown = false;
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        hovered = true;
    }

    @Override
    public void mouseExited(MouseEvent e) {
        hovered = false;
    }
}
