package Editor;

import java.awt.*;

class ColorVector
{
    public int r;
    public int g;
    public int b;

    public ColorVector(int r, int g, int b)
    {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public double getValue()
    {
        if (r + g + b > 0)
        {
            return 1;
        }
        return 0;
    }


}