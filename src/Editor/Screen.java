package Editor;

public class Screen
{
    public int Width;
    public int Height;
    ColorVector[][] pixels;

    public Screen(int width, int height)
    {
        pixels = new ColorVector[width][height];
        this.Width = width;
        this.Height = height;

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                pixels[i][j] = new ColorVector(0, 0, 0);
            }
        }
    }

    public void draw(int r, int g, int b, int x, int y)
    {
        pixels[x][y] = new ColorVector(r, g, b);
    }
    
    
    public double[] getNormalizedPixels()
    {
        double[] output = new double[Width * Height + 1];

        for (int i = 0; i < Width; i++)
        {
            for (int j = 0; j < Height; j++)
            {
                output[i + (j * Width)] = pixels[i][j].getValue();
            }
        }

        return output;
    }
    



}
