package Reader;
import java.io.BufferedReader; 
import java.io.FileNotFoundException; 
import java.io.FileReader; 
import java.io.IOException; 

public class Reader { 
	
	public static double [][] readX(String file, int numRows) throws FileNotFoundException, IOException {
		int row = 0;

		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		double x[][] = new double[numRows][785];
		row = 0; 
		while ((line = br.readLine()) != null && !line.isEmpty()) {
			String[] fields = line.split(",");
			for(int i=1; i < fields.length; i++) {
				x[row][i] = Double.parseDouble(fields[i]);
			}
			row++;
		}
				
		br.close();
		return x;
	}
	
	public static double [][] readY(String file, int numRows) throws FileNotFoundException, IOException {
		int row = 0;

		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		
		double y[][] = new double[numRows][10];
		
		while ((line = br.readLine()) != null && !line.isEmpty()) {
			String[] fields = line.split(",");
			
			y[row][Integer.parseInt(fields[0])] = 1;
				
			row++;
		}
				
		br.close();
		return y;
	}
	
}
