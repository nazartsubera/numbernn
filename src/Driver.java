import NeuralNet.NeuralNet;
import Reader.Reader;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.List;

import Editor.EditorWindow;

public class Driver {


    public static void main(String[] args) throws FileNotFoundException, IOException {

        double [][] X = Reader.readX("src/data/train.csv", 60000);
        double [][] Y = Reader.readY("src/data/train.csv", 60000);
        System.out.println(Y[1][0]);
    	
    	NeuralNet nn = new NeuralNet(785,100,10);


        List<Double>output;

        nn.fit(X, Y, 5000);

        EditorWindow window = new EditorWindow("Editor Window", 1000, 675);
        window.setup(nn);
        //test data function. not useful now but can be used to test 
        //overall accuracy of the network in the future.
        /*
        double [][] input = Reader.readX("src/data/test.csv", 10000);
        for(double d[]:input)
        {
            output = nn.predict(d);
            System.out.println(output.toString());
        }
        */

    }

}
